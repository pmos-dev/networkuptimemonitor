import { CommonsExpressServer } from 'nodecommons-express';
import { ICommonsExpressConfig } from 'nodecommons-express';
import { CommonsAppSocketIoServer } from 'nodecommons-app-socket-io';

export class SocketIoServer extends CommonsAppSocketIoServer {
	constructor(
			expressServer: CommonsExpressServer,
			expressConfig: ICommonsExpressConfig
	) {
		super(
				expressServer,
				expressConfig
		);
	}
	
//	public success(queueId: string): void {
//		this.broadcast('queue/success', { id: queueId });
//	}
//	
//	public fail(queueId: string, message: string): void {
//		this.broadcast('queue/fail', { id: queueId, message: message });
//	}
//	
//	public exception(queueId: string, e: Error): void {
//		this.broadcast('queue/exception', { id: queueId, message: e.message });
//	}
}
