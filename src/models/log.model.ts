import { ICommonsModel } from 'tscommons-models';

import { CommonsDatabaseTypeIdName } from 'nodecommons-database';
import { CommonsDatabaseTypeEnum } from 'nodecommons-database';
import { CommonsDatabaseTypeBase62BigId } from 'nodecommons-database';
import { CommonsDatabaseTypeDateTime } from 'nodecommons-database';
import { CommonsSqlDatabaseService } from 'nodecommons-database';
import { ECommonsDatabaseTypeNull } from 'nodecommons-database';
import { CommonsModel } from 'nodecommons-models';

import { TLastLogState } from '../types/tlast-log-state';

import { EServiceState, ESERVICE_STATES, fromEServiceState, toEServiceState } from '../enums/eservice-state';

export interface ILog extends ICommonsModel {
		uid: string;
		timestamp: Date;
		service: string;
		state: string;
}

export class LogModel extends CommonsModel<ILog> {
	
	constructor(
			database: CommonsSqlDatabaseService
	) {
		super(
				database,
				'logs',
				{
						uid: new CommonsDatabaseTypeDateTime(ECommonsDatabaseTypeNull.NOT_NULL),
						timestamp: new CommonsDatabaseTypeBase62BigId(ECommonsDatabaseTypeNull.NOT_NULL),
						service: new CommonsDatabaseTypeIdName(ECommonsDatabaseTypeNull.NOT_NULL),
						state: new CommonsDatabaseTypeEnum<EServiceState>(
								ESERVICE_STATES,
								fromEServiceState,
								toEServiceState,
								ECommonsDatabaseTypeNull.NOT_NULL
						)
				}
		);
	}

	protected preprepare(): void {
		super.preprepare();

//		const fields: string = Object.keys(this.structure)
//				.map((field: string): string => this.modelField(this, field))
//				.join(',');
		
		this.database.preprepare(
				'Uptime__Log__GET_LAST_STATE_BY_SERVICE',
				`
					SELECT ${this.modelField(this, 'uid')}, ${this.modelField(this, 'state')}, ${this.modelField(this, 'timestamp')}
					FROM ${this.model(this)}
					WHERE ${this.modelField(this, 'service')} = :service
				`,
				{
						service: this.structure['service']
				},
				{
						uid: this.structure['uid'],
						state: this.structure['state'],
						timestamp: this.structure['timestamp']
				}
		);
	}
	
	//-------------------------------------------------------------------------

	public async getLastStateByService(service: string): Promise<TLastLogState> {
		return await this.database.executeParamsValueNoNone<TLastLogState>(
				'Uptime__Log__GET_LAST_STATE_BY_SERVICE',
				{
						service: service
				}
		);
	}
	
	//---------------------------------------------------
	
	public async logForService(service: string, state: EServiceState): Promise<string> {
		const uid: string = CommonsDatabaseTypeBase62BigId.generateRandomBase62Id();
		
		await this.insert({
				uid: uid,
				timestamp: new Date(),
				service: service,
				state: state
		});
		
		return uid;
	}
}
