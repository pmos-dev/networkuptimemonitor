import { CommonsType } from 'tscommons-core';

import { IServiceConfig, isIServiceConfig } from '../interfaces/iservice-config';

export type TServicesConfig = {
		enabled: string[];
		configs: { [service: string ]: IServiceConfig[] };
};

export function isTServicesConfig(test: unknown): test is TServicesConfig {
	if (!CommonsType.hasPropertyStringArray(test, 'enabled')) return false;
	if (!CommonsType.hasPropertyTKeyObject<IServiceConfig>(test, 'configs', isIServiceConfig)) return false;
	
	return true;
}
