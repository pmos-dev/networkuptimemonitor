import { EServiceState } from '../enums/eservice-state';

export type TLastLogState = {
		uid: string;
		state: EServiceState;
		timestamp: Date;
};
