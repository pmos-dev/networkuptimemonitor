import { CommonsType } from 'tscommons-core';

export type TUptimeConfig = {
		pingInterval: number;
		lateAge: number;
};

export function isTUptimeConfig(test: unknown): test is TUptimeConfig {
	if (!CommonsType.hasPropertyNumber(test, 'pingInterval')) return false;
	if (!CommonsType.hasPropertyNumber(test, 'lateAge')) return false;
	
	return true;
}
