import { EServiceState } from '../enums/eservice-state';

export type TLogCallback = (
		service: string,
		state: EServiceState
) => void;
