import { CommonsType } from 'tscommons-core';

export type TNetworkConfig = {
		repeaters: string[];
		channel: string;
		pongExpiration?: number;
};

export function isTNetworkConfig(test: unknown): test is TNetworkConfig {
	if (!CommonsType.hasPropertyStringArray(test, 'repeaters')) return false;
	if (!CommonsType.hasPropertyString(test, 'channel')) return false;
	if (!CommonsType.hasPropertyNumberOrUndefined(test, 'pongExpiration')) return false;
	
	return true;
}
