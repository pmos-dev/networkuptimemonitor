import { Handler } from '../handlers/handler';

import { IServiceConfig } from '../interfaces/iservice-config';

import { TLogCallback } from '../types/tlog-callback';

// tslint:disable
export type THandlerCtor = {
		new(
				service: string,
				config: IServiceConfig,
				logCallback: TLogCallback
		): Handler;
};
