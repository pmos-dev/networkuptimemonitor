import { TPropertyObject } from 'tscommons-core';

import { EServiceState } from '../enums/eservice-state';

export type TPongCallback = (
		service: string,
		state: EServiceState,
		data?: TPropertyObject
) => void;
