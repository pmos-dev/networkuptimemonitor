import { TPropertyObject } from 'tscommons-core';

import { EServiceState } from '../enums/eservice-state';

export type TAttemptOutcome = {
		state: EServiceState;
		data?: TPropertyObject;
};
