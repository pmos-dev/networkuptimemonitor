import { CommonsOutput } from 'nodecommons-cli';

import { LogModel } from '../models/log.model';

import { EServiceState } from '../enums/eservice-state';

export class LogService {
	constructor(
			private logModel: LogModel
	) {}
	
	public async logForService(service: string, state: EServiceState): Promise<void> {
		const uid: string = await this.logModel.logForService(service, state);
		CommonsOutput.debug(`Last inserted log uid was ${uid}`);
	}
}
