import { Observable, Subject } from 'rxjs';

import { CommonsType } from 'tscommons-core';
import { ICommonsNetworkPacket } from 'tscommons-network';

import { CommonsOutput } from 'nodecommons-cli';

import { CommonsNetworkService } from 'networkcommons';
import { CommonsNetworkMultiSocketIoService } from 'networkcommons';

import { IPong, isIPong } from '../interfaces/ipong';

import { TNetworkConfig } from '../types/tnetwork-config';

export class NetworkService extends CommonsNetworkMultiSocketIoService {
	private onKeepAlive: Subject<void> = new Subject<void>();
	private onPong: Subject<IPong> = new Subject<IPong>();
	
	private networkConfig: TNetworkConfig;
	
	constructor(
			networkConfig: TNetworkConfig
	) {
		super(
				networkConfig.repeaters,
				false	// uncompressed
		);
		
		this.networkConfig = networkConfig;

		super.setDeviceId(CommonsNetworkMultiSocketIoService.generateDeviceId());

		super.receiveObservable().subscribe((packet: ICommonsNetworkPacket): void => {
			console.log(`${packet.ns}/${packet.command}`);
			switch (`${packet.ns}/${packet.command}`) {
				case 'network/keep-alive': {
					this.onKeepAlive.next();
					break;
				}

				case 'uptime/pong': {
					if (!isIPong(packet.data)) {
						console.log('Invalid operator/update packet');
						return;
					}
					
					this.onPong.next(packet.data);
					break;
				}
			}
		});
		
		const channel: string = CommonsType.assertString(networkConfig.channel);
		this.setChannel(channel);
		
		this.connectObservable()
				.subscribe((): void => {
					CommonsOutput.debug('Socket.io has connected');
				});
		
		this.disconnectObservable()
				.subscribe((): void => {
					CommonsOutput.debug('Socket.io has disconnected');
				});
	}

	public triggerPeerNumberChanged(): void {
		// connect happens before ngInit sometimes
		super.triggerPeerNumberChanged();
	}
	
	public keepAliveObservable(): Observable<void> {
		return this.onKeepAlive;
	}

	public pongObservable(): Observable<IPong> {
		return this.onPong;
	}
	
	public async keepAlive(): Promise<void> {
		const deviceId: string|undefined = this.getDeviceId();
		if (!deviceId) return;

		await super.direct(deviceId, 'network', 'keep-alive', {}, CommonsNetworkService.expiryDate(10));
	}
	
	public async pong(packet: IPong): Promise<void> {
		let expiration: Date|undefined;
		if (this.networkConfig.pongExpiration !== undefined) {
			expiration = new Date(new Date().getTime() + this.networkConfig.pongExpiration);
		}
		
		CommonsOutput.debug('Broadcasting packet to network');
		await super.broadcast(
				'uptime',
				'pong',
				packet,
				expiration,
				false	// don't ignore own
		);
	}
}
