import { TPropertyObject } from 'tscommons-core';

import { IServiceConfig } from '../interfaces/iservice-config';

import { TLogCallback } from '../types/tlog-callback';

import { EServiceState } from '../enums/eservice-state';

export abstract class Handler {
	constructor(
			private service: string,
			protected config: IServiceConfig,
			private logCallback: TLogCallback
	) {}

	protected log(
		state: EServiceState
	): void {
		this.logCallback(this.service, state);
	}
	
	public abstract pong(
			state: EServiceState,
			data?: TPropertyObject
	): void;
}
