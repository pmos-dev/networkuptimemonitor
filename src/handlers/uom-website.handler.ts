import { IWebsiteServiceConfig } from '../interfaces/iwebsite-service-config';

import { TLogCallback } from '../types/tlog-callback';

import { WebsiteHandler } from './website.handler';

export abstract class UomWebsiteHandler extends WebsiteHandler {
	constructor(
			service: string,
			config: IWebsiteServiceConfig,
			logCallback: TLogCallback
	) {
		super(service, config, logCallback);
	}
}
