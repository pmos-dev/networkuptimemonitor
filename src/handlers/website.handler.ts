import { TPropertyObject } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';

import { IWebsiteServiceConfig, isIWebsiteServiceConfig } from '../interfaces/iwebsite-service-config';

import { TLogCallback } from '../types/tlog-callback';

import { EServiceState } from '../enums/eservice-state';

import { Handler } from './handler';

export abstract class WebsiteHandler extends Handler {
	constructor(
			service: string,
			protected config: IWebsiteServiceConfig,
			logCallback: TLogCallback
	) {
		super(service, config, logCallback);
		if (!isIWebsiteServiceConfig(config)) throw new Error('WebsiteHandler config is not valid post-super constructor');
	}
	
	public pong(
			state: EServiceState,
			_?: TPropertyObject
	): void {
		CommonsOutput.debug(`${this.config.url} reported to be ${state}`);
		
		this.log(state);
	}
}
