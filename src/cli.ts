#!/usr/bin/env node

import { CommonsConfig } from 'tscommons-config';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { isICommonsCredentials } from 'nodecommons-database';
import { CommonsPostgresService } from 'nodecommons-database-postgres';

import { UptimeApp } from './apps/uptime.app';

const args: CommonsArgs = new CommonsArgs();
if (args.hasAttribute('debug')) CommonsOutput.setDebugging(true);
if (args.hasAttribute('daemon')) CommonsOutput.setDaemon(true);

const app: UptimeApp = new UptimeApp();

app.autoSystemd(
		'network-uptime-monitor',
		'Node.js service uptime monitor using network repeater'
);

const configAuth: CommonsConfig = app.loadConfigFile('database-auth.json');
const credentials: unknown = configAuth.getObject('database');
if (!isICommonsCredentials(credentials)) throw new Error('Database credentials are not valid');

const databaseService: CommonsPostgresService = new CommonsPostgresService(credentials);
app.setDatabaseService(databaseService);

(async (): Promise<void> => {
	CommonsOutput.info('Starting application: Uptime');
	await app.start();
	CommonsOutput.info('Application completed');
	
	setTimeout((): void => {
		//log() // logs out active handles that are keeping node running
		process.exit(0);
	}, 100);
})();
