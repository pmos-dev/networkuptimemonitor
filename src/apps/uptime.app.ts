import { TPropertyObject } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';
import { CommonsArgs } from 'nodecommons-cli';
import { CommonsExpressServer } from 'nodecommons-express';
import { ICommonsExpressConfig } from 'nodecommons-express';
import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsSqlDatabaseService } from 'nodecommons-database';
import { CommonsSocketIoApp } from 'nodecommons-app-socket-io';

import { PongApi } from '../apis/pong.api';

import { Handler } from '../handlers/handler';

import { LogModel } from '../models/log.model';

import { SocketIoServer } from '../servers/socket-io.server';

import { NetworkService } from '../services/network.service';
import { LogService } from '../services/log.service';

import { IServiceConfig } from '../interfaces/iservice-config';
import { IPong } from '../interfaces/ipong';

import { isTServicesConfig } from '../types/tservices-config';
import { isTNetworkConfig } from '../types/tnetwork-config';
import { THandlerCtor } from '../types/thandler-ctor';

import { EServiceState } from '../enums/eservice-state';

type TRequiredHandler = {
		[ name: string ]: THandlerCtor;
};

export class UptimeApp extends CommonsSocketIoApp<SocketIoServer> {
	private databaseService: CommonsSqlDatabaseService|undefined;
	private networkService: NetworkService;
	private logModel: LogModel|undefined;
	private logService: LogService|undefined;
	
	private handlers: Map<string, Handler> = new Map<string, Handler>();
	
	constructor() {
		super(
				'network-uptime-monitor',
				true
		);

		const networkConfig: unknown = this.getConfigArea('network');
		if (!isTNetworkConfig(networkConfig)) {
			CommonsOutput.error('Invalid network config');
			process.exit(1);
			throw(new Error());	// typescript
		}
		
		this.networkService = new NetworkService(networkConfig);
	}
	
	protected buildSocketIoServer(
			expressServer: CommonsExpressServer,
			expressConfig: ICommonsExpressConfig
	): SocketIoServer {
		return new SocketIoServer(
				expressServer,
				expressConfig
		);
	}

	public setDatabaseService(
			databaseService: CommonsSqlDatabaseService
	): void {
		this.databaseService = databaseService;
	}
	
	public async init(): Promise<void> {
		if (!this.databaseService) throw new Error('Database service has not been set yet');

		CommonsOutput.doing('Connecting to database');
		await this.databaseService.connect();
		CommonsOutput.success();
		
		CommonsOutput.doing('Initialising models');
		this.logModel = new LogModel(this.databaseService);
		this.logModel.init();
		CommonsOutput.success();
		
		this.logService = new LogService(this.logModel);

		const servicesConfig: unknown = this.getConfigArea('services');
		if (!isTServicesConfig(servicesConfig)) throw new Error('Invalid TServicesConfig');

		for (const service of servicesConfig.enabled) {
			if (!/^[-a-z0-9]+$/.test(service)) {
				CommonsOutput.error(`Refusing to install invalid service name ${service}`);
				continue;
			}
			
			CommonsOutput.debug(`Installing handler for ${service}`);
			const handlerImport: TRequiredHandler = require(`../handlers/${service}.handler.js`);
			const handlerCtor: THandlerCtor = handlerImport[Object.keys(handlerImport)[0]];
			
			const serviceConfig: IServiceConfig|undefined = servicesConfig.configs[service];
			
			const handler: Handler = new handlerCtor(
					service,
					serviceConfig,
					(
							s: string,
							state: EServiceState
					): void => this.log(s, state)
			);
			this.handlers.set(service, handler);
		}

		// the actual receiving of pongs is via the repeaters, and the direct API calls are just to trigger the repeating broadcast
		
		this.networkService.pongObservable()
				.subscribe((pong: IPong): void => {
					if (!this.handlers.has(pong.service)) {
						CommonsOutput.debug(`No such service handler for ${pong.service}`);
						return;
					}
					
					const handler: Handler = this.handlers.get(pong.service)!;
					handler.pong(pong.state, pong.data);
				});

		CommonsOutput.doing('Setting up APIs');
		this.install(
				(restServer: CommonsRestServer, path: string): void => {
					// tslint:disable-next-line:no-unused-expression
					new PongApi(
							restServer,
							path,
							servicesConfig,
							(	// direct pong callback, not repeated
									service: string,
									state: EServiceState,
									data?: TPropertyObject
							): void => this.triggerPong(service, state, data)
					);
				}
		);
		CommonsOutput.success();
		
		await super.init();
		
		// this is going to be for browser clients, not the repeaters
		const socketIoServer: SocketIoServer|undefined = this.getSocketIoServer();
		if (!socketIoServer) throw new Error('SocketIoServer server has not been set. This should not be possible');
	}
	
	private triggerPong(
			service: string,
			state: EServiceState,
			data?: TPropertyObject
	): void {
		CommonsOutput.debug(`Sending network pong broadcast for ${service}`);
		this.networkService.pong({
				service: service,
				state: state,
				data: data
		});
	}
	
	private log(
			service: string,
			state: EServiceState
	): void {
		if (!this.logService) return;	// not actually possible
		
		this.logService.logForService(service, state);
	}
	
	public async run(): Promise<void> {
		const args: CommonsArgs = this.getArgs();
		
		if (!this.logModel) throw new Error('LogModel has not been created. This should not be possible');
		
		if (args.hasAttribute('drop')) {
			CommonsOutput.doing('Dropping tables');
			try {
				await this.logModel.dropTable();
				CommonsOutput.success();
			} catch (e) {
				CommonsOutput.fail();
			}
		}
	
		if (args.hasAttribute('create')) {
			CommonsOutput.doing('Creating tables');
			try {
				await this.logModel.createTable();
				CommonsOutput.success();
			} catch (e) {
				CommonsOutput.fail();
			}
		}
		
		CommonsOutput.doing('Starting network service');
		this.networkService.connect();
		CommonsOutput.success();
		
		await super.run();
	}
	
	protected async shutdown(): Promise<void> {
		//if (!this.convertServer) throw new Error('ConvertServer has not been instantiated. This should not be possible');

		//this.convertServer.abort();
		//await this.convertServer.shutdown();
		
		await super.shutdown();
	}
}
