import { CommonsType } from 'tscommons-core';

export enum EServiceState {
		UP = 'up',
		DOWN = 'down',
		LATE = 'late',
		ISSUES = 'issues'
}

export function keyToEServiceState(value: string): EServiceState {
	switch (value) {
		case 'UP':
			return EServiceState.UP;
		case 'DOWN':
			return EServiceState.DOWN;
		case 'LATE':
			return EServiceState.LATE;
		case 'ISSUES':
			return EServiceState.ISSUES;
	}

	throw new Error('Unknown EServiceState key');
}

export function fromEServiceState(state: EServiceState): string {
	switch (state) {
		case EServiceState.UP:
			return EServiceState.UP.toString();
		case EServiceState.DOWN:
			return EServiceState.DOWN.toString();
		case EServiceState.LATE:
			return EServiceState.LATE.toString();
		case EServiceState.ISSUES:
			return EServiceState.ISSUES.toString();
	}
	
	throw new Error('Unknown EServiceState');
}

export function toEServiceState(value: string): EServiceState|undefined {
	switch (value) {
		case EServiceState.UP.toString():
			return EServiceState.UP;
		case EServiceState.DOWN.toString():
			return EServiceState.DOWN;
		case EServiceState.LATE.toString():
			return EServiceState.LATE;
		case EServiceState.ISSUES.toString():
			return EServiceState.ISSUES;
	}
	
	return undefined;
}

export function isEServiceState(test: unknown): test is EServiceState {
	if (!CommonsType.isString(test)) return false;
	
	return toEServiceState(test) !== undefined;
}

export const ESERVICE_STATES = Object.keys(EServiceState)
		.map((key: string): EServiceState => keyToEServiceState(key));
