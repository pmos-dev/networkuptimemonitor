// tslint:disable-next-line:no-empty-interface
export interface IServiceConfig {}

export function isIServiceConfig(_test: unknown): _test is IServiceConfig {
	return true;
}
