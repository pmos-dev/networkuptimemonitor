import { CommonsType } from 'tscommons-core';

import { IServiceConfig } from './iservice-config';

export interface IWebsiteServiceConfig extends IServiceConfig {
		url: string;
}

export function isIWebsiteServiceConfig(test: unknown): test is IWebsiteServiceConfig {
	if (!CommonsType.hasPropertyString(test, 'url')) return false;

	return true;
}
