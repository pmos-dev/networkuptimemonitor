import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';

import { EServiceState, isEServiceState } from '../enums/eservice-state';

export interface IPong {
		service: string;
		state: EServiceState;
		data?: TPropertyObject;
}

export function isIPong(test: unknown): test is IPong {
	if (!CommonsType.hasPropertyString(test, 'service')) return false;
	if (!CommonsType.hasPropertyEnum<EServiceState>(test, 'service', isEServiceState)) return false;
	if (!CommonsType.hasPropertyObjectOrUndefined(test, 'data')) return false;
	
	return true;
}
