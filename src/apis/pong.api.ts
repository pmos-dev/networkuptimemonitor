import * as express from 'express';

import { CommonsType } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';
import { ICommonsRequestWithStrictParams } from 'nodecommons-express';
import { CommonsRestServer } from 'nodecommons-rest';
import { CommonsRestApi } from 'nodecommons-rest';

import { TServicesConfig } from '../types/tservices-config';
import { TPongCallback } from '../types/tpong-callback';

import { EServiceState, isEServiceState } from '../enums/eservice-state';

export class PongApi extends CommonsRestApi {
	constructor(
			restServer: CommonsRestServer,
			path: string,
			config: TServicesConfig,
			triggerPongCallback: TPongCallback
	) {
		super(restServer);
		
		super.patchHandler(
				`${path}pong/:service[idname]`,
				async (req: ICommonsRequestWithStrictParams, _res: express.Response): Promise<void> => {
					const service: string = req.strictParams.service as string;
					CommonsOutput.debug(`Received pong from ${service}`);
					
					if (!config.enabled.includes(service)) {
						CommonsOutput.error(`Service does not exist for ${service}`);
						return;
					}
					
					let state: EServiceState = EServiceState.UP;
					if (CommonsType.hasPropertyEnum<EServiceState>(req.body, 'state', isEServiceState)) state = req.body.state;
					
					CommonsOutput.debug(`Triggering pong callback for ${service} with state ${state}`);
					triggerPongCallback(service, state, req.body);
				}
		);
	}
}
