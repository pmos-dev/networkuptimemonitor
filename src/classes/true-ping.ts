// tslint:disable-next-line
const netPing = require('net-ping');

import { CommonsOutput } from 'nodecommons-cli';

//import { NetworkService } from '../services/network.service';

import { TPongCallback } from '../types/tpong-callback';
import { TAttemptOutcome } from '../types/tattempt-outcome';

import { EServiceState } from '../enums/eservice-state';

import { Ping } from './ping';

type TSession = {
		pingHost: (
				dest: string,
				callback: (
						error: Error,
						target: unknown
				) => void
		) => void;
};

export abstract class TruePing extends Ping {
	private static pingAsync(dest: string): Promise<boolean> {
		return new Promise((
				resolve: (success: boolean) => void,
				_reject: (e: Error) => void
		) => {
			const session: TSession = netPing.createSession();
			session.pingHost(
					dest,
					(error: Error, _target: unknown): void => {
						if (error) {
							resolve(false);
						}
						
						resolve(true);
					}
			);
		});
	}
	
	constructor(
			serviceName: string,
			private remoteAddr: string,
			triggerPongCallback: TPongCallback
	) {
		super(
				serviceName,
				triggerPongCallback
		);
	}
	
	public async attempt(): Promise<TAttemptOutcome> {
		CommonsOutput.debug(`Attempting ping of ${this.remoteAddr}`);
		const outcome: boolean = await TruePing.pingAsync(this.remoteAddr);
		CommonsOutput.debug(`Outcome was ${outcome}`);
			
		if (outcome) {
			return { state: EServiceState.UP };
		} else {
			return { state: EServiceState.DOWN };
		}
	}
}
