import { TPropertyObject } from 'tscommons-core';

import { CommonsOutput } from 'nodecommons-cli';

//import { NetworkService } from '../services/network.service';

import { TPongCallback } from '../types/tpong-callback';
import { TAttemptOutcome } from '../types/tattempt-outcome';

import { EServiceState } from '../enums/eservice-state';

export abstract class Ping {
	constructor(
			private serviceName: string,
			private triggerPongCallback: TPongCallback
			
//			_networkService: NetworkService
	) {}
	
	protected pong(
		state: EServiceState,
		data?: TPropertyObject
	): void {
		this.triggerPongCallback(this.serviceName, state, data);
	}
	
	protected abstract attempt(): Promise<TAttemptOutcome>;
	
	public ping(): void {
		this.attempt()
				.then((outcome: TAttemptOutcome): void => {
					this.pong(outcome.state, outcome.data);
				})
				.catch((e: Error): void => {
					CommonsOutput.error(e.message);
					this.pong(
							EServiceState.ISSUES,
							{
								message: 'Service ping failed'
							}
					);
				});
	}
}
